-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2020 at 07:46 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `magebit`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `FirstName` varchar(64) NOT NULL,
  `LastName` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `age` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `FirstName`, `LastName`, `email`, `password`, `age`) VALUES
(5, 'Eriks', '', 'bigeriks@gmail.com', '$2y$10$uIJmtqS4R36tDLJXqWd9RuUdZdOqMJvAlISMEO34SWVssPfFbyF5.', 0),
(6, 'Hammer', '', 'hammeriszebest@iamright.com', '$2y$10$DbV40Q4z/9xeuJDGascUM.LddA8ZzTBxMfLETQ8vKnK3KD7GwXXyu', 0),
(7, 'IAmATree', '', 'tree@tree.com', '$2y$10$g6Hi9Yf8.S03EEwzKCNBBe8KSyAx5XEsL.St2divxHQvbqb1mR4hC', 0),
(8, 'Eriks', '', 'eriks@gmail.com', '$2y$10$UJVnXTyN6Ty7LC02zZ5bku5VNbXyzDJHjO30VBFJGSGPOUcVOP6LK', 0),
(9, 'asd', 'asd', 'asd@asd.com', '$2y$10$PDX49P/XzKYXN.16X1iNau5NKBwMU7eEhSsHOCghPmUCXD8Ipswae', 5),
(10, 'Uepe', '', 'upe@upe.lv', '$2y$10$NMtfj6WC9d9tvXRVhBxD7u/nj7vmps7Bv1u6p4nr7cy9K/U.Alqvy', 0),
(11, 'AMurs', '', 'amurs@amurs.lv', '$2y$10$ZCrToWESZ4I7Q.1uCOaQoOuSt76FqCl8/F3xaYC1EYW/5FXMGvrLC', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
