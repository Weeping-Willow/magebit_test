<?php

include_once "../classes.php";
class ValidateUserInput
{
    private $password;
    private $email;
    /**
     * @var User
     */
    private User $user;
    private $name;
    private $age;
    /**
     * @var string
     */
    private string $firstName;
    public string $lastName;
    public function __construct($whichInput)
    {
        if($this->dataIsSubmited())
        {

            if ($whichInput === "login")
            {
                $this->email =strtolower($_POST['lEmail']);
                $this->password = $_POST['lPassword'];
                $this->login();

            }
            else if($whichInput === "edit")
            {

                $this->firstName = strtolower($_POST['firstName']);
                $this->lastName = $_POST['lastName'];
                $this->age = $_POST['age'];

                $this->edit();



            }
            else
                {
                    $this->email = strtolower($_POST['sEmail']);
                    $this->password = $_POST['sPassword'];
                    $this->name = $_POST['sName'];

                    $this->signUp();
                }

        }
    }

    private function dataIsSubmited()
    {
        return  isset($_POST['Submit']) || isset($_POST['loginSubmit'])||isset($_POST['submit']);
    }

    private function isEmailValid()
    {
        return filter_var($this->email,FILTER_VALIDATE_EMAIL);
    }

    private function isPasswordTooLong()
    {
        return  strlen($this->password) > 64;
    }

    private function isEmailTooLong()
    {
        return strlen($this->email) > 100;
    }

    private function inputIsEmpty($whichInput)
    {
        if ($whichInput ==="login")
        {
            return empty($this->email) || empty($this->password);
        }
        else
            {
                return empty($this->email) || empty($this->password)||empty($this->name);
            }

    }

    function login()
    {
        if ($this->inputIsEmpty("login"))
        {
            header("Location: ../view/Index.php?error=emptyfields&lEmail='.$this->email.'");
            exit();
        }
        else if(!$this->isEmailValid())
        {
            header("Location: ../view/Index.php?error=invalidemail&lEmail='.$this->email.'");
            exit();
        }
        else if($this->isPasswordTooLong())
        {
            header("Location: ../view/Index.php?error=invalidemailorpass&lEmail='.$this->email.'");
            exit();
        }
        else if($this->isEmailTooLong())
        {
            header("Location: ../view/Index.php?error=invalidemailorpass&lEmail='.$this->email.'");
            exit();
        }
        else
        {
            $this->user = new ExistingUser($this->email,$this->password);


            if($this->user->authenticate())
            {
                header("Location: ../view/Index.php?error=errorsql&lEmail='.$this->email.'");
                exit();
            }
            else
                {
                    header("Location: ../view/loggedOn.php");
                   exit();
                }
        }
    }

    function signUp()
    {
        if ($this->inputIsEmpty("sign up"))
        {
            header("Location: ../view/Index.php?error=emptyfields&sEmail='.$this->email.'&sName = '.$this->name.'");
            exit();
        }
        else if(!$this->isEmailValid())
        {
            header("Location: ../view/Index.php?error=invalidemail&sEmail='.$this->email.'&sName = '.$this->name.'");
            exit();
        }
        else if($this->isPasswordTooLong())
        {
            header("Location: ../view/Index.php?error=PasswordTooLong&sEmail='.$this->email.'&sName = '.$this->name.'");
            exit();
        }
        else if($this->isEmailTooLong())
        {
            header("Location: ../view/Index.php?error=EmailTooLong&sEmail='.$this->email.'&sName = '.$this->name.'");
            exit();
        }
        else if($this->isNameTooLong())
        {
            header("Location: ../view/Index.php?error=nameTooLong&sEmail='.$this->email.'&sName = '.$this->name.'");
            exit();
        }
        else
        {
            $this->user = new NewUser($this->email,$this->password,$this->name);
            $authenticateResponse =  $this->user->create();
            if(!$authenticateResponse)
            {
                header("Location: ../view/Index.php?error=emailexists&sEmail='.$this->email.'&sName = '.$this->name.'");
                exit();
            }
            else
                {
                    header("Location: ../view/loggedOn.php");
                    exit();
                }
        }
    }

    function edit()
    {
        $this->user = new ExistingUserUpdate($this->firstName,$this->lastName,$this->age);
        $this->user->update();
        header("Location: ../view/loggedOn.php");
        exit();
    }
    private function isNameTooLong()
    {
        return strlen($this->name) > 64;
    }
}

