let side=1;

function changeSName () {
    let x = document.getElementById("sName");
    let curVal = x.value;
    let imageRW = document.getElementById('img-sname');
    if (curVal === "") {
        //wrong
        imageRW.src = "Media/Name_logo_grey.png";
    } else {
        //right
        imageRW.src = "Media/Name_logo_blue.png";
    }
}

function changeLEmail()
{
    let x = document.getElementById("lEmail");
    let curVal = x.value;
    let imageRW = document.getElementById('img-lEmail');
    if (curVal === "") {
        //wrong
        imageRW.src = "Media/Mail_logo_grey.png";
    } else {
        //right
        imageRW.src = "Media/Mail_logo_blue.png";
    }
}

function changeLPassword()
{
    let x = document.getElementById("lPassword");
    let curVal = x.value;
    let imageRW = document.getElementById('img-lPassword');
    if (curVal === "") {
        //wrong
        imageRW.src = "Media/Password_logo_grey.png";
    } else {
        //right
        imageRW.src = "Media/Password_logo_blue.png";
    }
}

function changeSPassword()
{
    let x = document.getElementById("sPassword");
    let curVal = x.value;
    let imageRW = document.getElementById('img-sPassword');
    if (curVal === "") {
        //wrong
        imageRW.src = "Media/Password_logo_grey.png";
    } else {
        //right
        imageRW.src = "Media/Password_logo_blue.png";
    }
}

function changeSEmail()
{
    let x = document.getElementById("sEmail");
    let curVal = x.value;
    let imageRW = document.getElementById('img-sEmail');
    if (curVal === "") {
        //wrong
        imageRW.src = "Media/Mail_logo_grey.png";
    } else {
        //right
        imageRW.src = "Media/Mail_logo_blue.png";
    }
}



Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(let i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}


function marginChange() {
    let temp= document.getElementById("infoInputScreen");
    if (temp.style.marginLeft ==="550px" )
    {
        temp.style.marginLeft="0px";
    }
    else
        {
            temp.style.marginLeft="550px";
        }

}
function loginScreenToSignUp() {
    side = 0;
    transitionLoginToSignUp();
    let screenWidth = innerWidth;
    if (screenWidth < 993)
    {
        document.getElementById("infoInputScreen").style.marginTop="-150px";
        document.getElementById("infoInputScreen").style.position="300";

    }
    else
        {
            document.getElementById("infoInputScreen").style.marginTop="0px";
        }
}

function signUpScreenToLogin() {
    side = 1;
    transitionSignUpToLogin();
    let screenWidth = innerWidth;
    if (screenWidth < 993)
    {
        document.getElementById("infoInputScreen").style.marginTop="220px";
        document.getElementById("infoInputScreen").style.position="300";
    }
    else
        {
            document.getElementById("infoInputScreen").style.marginTop="0px";
        }
}
function transitionSignUpToLogin() {
    let elem = document.getElementById("infoInputScreen");
    let pos = 0;
    let id = setInterval(frame, 10);
    function frame() {
        if (pos === 550) {
            clearInterval(id);
        } else {
            pos = pos+5;
            elem.style.marginLeft = pos + 'px';
        }
        if (pos === 360)
        {
            document.getElementById("blank").remove();
            addLogin();
        }
        else if(pos===265)
        {
            document.getElementById("sign_up").remove();
            addBlank();
        }
    }
}

function transitionLoginToSignUp() {
    let elem = document.getElementById("infoInputScreen");
    let pos = 550;
    let id = setInterval(frame, 10);
    function frame() {
        if (pos === 0) {
            clearInterval(id);
        } else {
            pos = pos-5;
            elem.style.marginLeft = pos + 'px';
        }
        if (pos === 180)
        {
            document.getElementById("blank").remove();
            addSignUp();
        }
        else if(pos===275)
        {
            document.getElementById("login").remove();
            addBlank();
        }
    }
}

function addLogin() {
    document.querySelector('#infoInputScreen').insertAdjacentHTML(
        'afterbegin',
        `<div class="login" id="login">
            <form  action="../Controller/loginrReceive.php" method="post">
                <div class="line-front-first">
                    <div class="text-front">
                        <p>Login</p>
                    </div>
                    <div class="logo-signup">
                        <img src="Media/main-logo.png" alt="">
                    </div>
                </div>
                <hr>
                <div class="front-line-text">
                    <div class="text-front">
                        <p>Email<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-lEmail" src="Media/Mail_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="text" id="lEmail" onchange="changeLEmail()" name="lEmail"><br><br>
                </div>
                <div class="front-line-text">
                    <div class="text-front">
                        <p>Password<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-lPassword" src="Media/Password_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="password" id="lPassword" onchange="changeLPassword()" name="lPassword"><br><br>
                </div>
                <button type="submit" class="frontScreenButton" name="Submit">LOGIN</button>
            </form>
        </div>`
    );
}

function addSignUp() {
    document.querySelector('#infoInputScreen').insertAdjacentHTML(
        'afterbegin',
        ` <div class="sign_up" id="sign_up">
            <form action="../Controller/signUpReceive.php" method="post">
                <div class="line-front-first">
                    <div class="text-front">
                        <p>Sign Up</p>
                    </div>
                    <div class="logo-signup">
                        <img src="Media/main-logo.png" alt="">
                    </div>
                </div>
                <hr>

                <div class="front-line-text">
                    <div class="text-front">
                        <p>Name<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-sname" src="Media/Name_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="text" id="sName" onchange="changeSName()" name="sName"><br><br>
                </div>

                <div class="front-line-text">
                    <div class="text-front">
                        <p>Email<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-sEmail" src="Media/Mail_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="text" id="sEmail" onchange="changeSEmail()" name="sEmail"><br><br>
                </div>

                <div class="front-line-text">
                    <div class="text-front">
                        <p>Password<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-sPassword" src="Media/Password_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="password" id="sPassword" onchange="changeSPassword()" name="sPassword"><br><br>
                </div>
                <button type="submit" class="frontScreenButton" name="submit">SIGN UP</button>

            </form>
        </div>`
    );
}

function addBlank() {
    document.querySelector('#infoInputScreen').insertAdjacentHTML(
    'afterbegin',
        `<div class="login" id="blank">
                
            </div>`
);
}

function onResize() {
    let elem = document.getElementById("infoInputScreen");

    if (elem < 992)
    {
        elem.style.marginTop="0px";
    }
    else
        {
            elem.style.marginTop="220px";
        }

}