
<?php
include_once "../classes.php";


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script type="text/javascript" src="js/functions.js"></script>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/reset.css">
</head>
<body >
<div class="main">
    <div class="backScreen">
        <div class="left">
            <div class="box">

        </div>
        <div class="right">
            <div class="box">

            </div>
        </div>
    </div>
</div>
    <section id="cool"  class="infoInputScreen">

        <div class="loggedOn" id="loggedOn">
            <form action="../Controller/loggedOnEditReceive.php" method="post">
                <div class="line-front-first">
                    <div class="text-front">
                        <p>Edit</p>
                    </div>
                    <div class="logo-signup">
                        <img src="Media/main-logo.png" alt="">
                    </div>
                </div>
                <hr>

                <div class="front-line-text">
                    <div class="text-front">
                        <p>First name<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                    </div>
                </div>
                <div class="input">
                    <input type="text" maxlength="64"  placeholder="<?php echo $_SESSION["FirstName"]; ?>" id="sName"  name="firstName"><br><br>
                </div>

                <div class="front-line-text">
                    <div class="text-front">
                        <p>Last name<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                    </div>
                </div>
                <div class="input">
                    <input type="text" maxlength="64" id="sEmail" placeholder="<?php echo $_SESSION["LastName"]; ?>" name="lastName"><br><br>
                </div>

                <div class="front-line-text">
                    <div class="text-front">
                        <p>Age<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">

                    </div>
                </div>
                <div class="input">
                    <input type="number" maxlength="11"  placeholder="<?php echo $_SESSION["age"]; ?>" id="sPassword" name="age"><br><br>
                </div>

                <button type="submit" class="frontScreenButton" name="submit">Save</button>
                <a href="Index.php" class="frontScreenButton">LOGOUT</a>

            </form>
        </div>
    </section>
<noscript>Sorry, your browser does not support JavaScript!</noscript>
</body>
</html>
