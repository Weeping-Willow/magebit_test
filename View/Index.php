
<?php
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script type="text/javascript" src="js/functions.js"></script>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/reset.css">
</head>
<body onresize="onResize()">
<div class="main">
    <div class="backScreen">
        <div class="left">
            <div class="box">
                <div class="headtitle">
                    <p>Don't have an account?</p>
                </div>
                <hr>
                <div class="headtext">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquam consequat lectus nec
                        elementum. Phasellus velit turpis, mattis ut sapien at, gravida sollicitudin felis. In at
                        malesuada dolor.</p>
                </div>
                <button  onclick="loginScreenToSignUp()"  class="backscreenButton">SIGN UP</button>
            </div>
        </div>
        <div class="right">
            <div class="box">
                <div class="headtitle">
                    <p>Have an account?</p>
                </div>
                <hr>
                <div class="headtext">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquam consequat lectus nec
                        elementum.</p>
                </div>
                <button onclick="signUpScreenToLogin()" class="backscreenButton">LOGIN</button>
            </div>
        </div>
    </div>

    <section id="infoInputScreen" onload="login_margin_change()" class="infoInputScreen">

        <div class="sign_up" id="sign_up">
            <form action="../Controller/signUpReceive.php" method="post">
                <div class="line-front-first">
                    <div class="text-front">
                        <p>Sign Up</p>
                    </div>
                    <div class="logo-signup">
                        <img src="Media/main-logo.png" alt="">
                    </div>
                </div>
                <hr>

                <div class="front-line-text">
                    <div class="text-front">
                        <p>Name<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-sname" src="Media/Name_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="text" maxlength="64"  id="sName" onchange="changeSName()" name="sName"><br><br>
                </div>

                <div class="front-line-text">
                    <div class="text-front">
                        <p>Email<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-sEmail" src="Media/Mail_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="text" maxlength="100"  id="sEmail" onchange="changeSEmail()" name="sEmail"><br><br>
                </div>

                <div class="front-line-text">
                    <div class="text-front">
                        <p>Password<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-sPassword" src="Media/Password_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="password" maxlength="64"  id="sPassword" onchange="changeSPassword()" name="sPassword"><br><br>
                </div>
                <button type="submit" class="frontScreenButton" name="submit">SIGN UP</button>

            </form>
        </div>

        <div class="login" id="login">
            <form  action="../Controller/loginrReceive.php" method="post">
                <div class="line-front-first">
                    <div class="text-front">
                        <p>Login</p>
                    </div>
                    <div class="logo-signup">
                        <img src="Media/main-logo.png" alt="">
                    </div>
                </div>
                <hr>
                <div class="front-line-text">
                    <div class="text-front">
                        <p>Email<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-lEmail" src="Media/Mail_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="text" maxlength="100"  id="lEmail" onchange="changeLEmail()" name="lEmail"><br><br>
                </div>
                <div class="front-line-text">
                    <div class="text-front">
                        <p>Password<span class="asterik">*</span></p>
                    </div>
                    <div class="logo-signup">
                        <img id="img-lPassword" src="Media/Password_logo_grey.png" alt="">
                    </div>
                </div>
                <div class="input">
                    <input type="password" maxlength="64"  id="lPassword" onchange="changeLPassword()" name="lPassword"><br><br>
                </div>
                <button type="submit" class="frontScreenButton" name="loginSubmit">LOGIN</button>
            </form>
        </div>
        <script>
            document.getElementById("sign_up").remove();
            marginChange()
        </script>

    </section>
</div>

<noscript>Sorry, your browser does not support JavaScript!</noscript>
</body>
</html>