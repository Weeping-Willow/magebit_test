<?php

include_once "../classes.php";
session_start();
class DatabaseConn
{
    private $databaseHost = "localhost";
    private $databseUser = "root";
    private $databsePassword ="";
    private $databaseName = "magebit";

    protected function connect()
    {
        return mysqli_connect($this->databaseHost,$this->databseUser,$this->databsePassword,$this->databaseName);
    }

    public function authenticateUserCredentials($email,$password)
    {

        $databaseConnection = $this->connect();

        if ($databaseConnection->connect_error) {
            return false;
        }

        $sql = "Select * from user WHERE email=?";

        $sqlStatement = mysqli_stmt_init($databaseConnection);

        if (!mysqli_stmt_prepare($sqlStatement, $sql)) {
            return false;
        } else {

            mysqli_stmt_bind_param($sqlStatement, 's', $email);
            mysqli_stmt_execute($sqlStatement);

            $resultOfSqlQuery = mysqli_stmt_get_result($sqlStatement);


            if ($row = mysqli_fetch_assoc($resultOfSqlQuery)) {

                $passwordMatches = password_verify($password, $row['password']);
                if ($passwordMatches == false) {
                    $sqlStatement->close();
                    $databaseConnection->close();

                    return false;
                } else if ($passwordMatches == true) {


                    $_SESSION['Email'] = $email;
                    $_SESSION['FirstName'] = $row['FirstName'];
                    $_SESSION['LastName'] = $row['LastName'];
                    $_SESSION['age'] = $row['age'];

                    $sqlStatement->close();
                    $databaseConnection->close();

                    return true;
                }
            } else {
                $sqlStatement->close();
                $databaseConnection->close();

                return false;
            }

        }
    }

    public function checkForExistingEmail($email)
    {
        $databaseConnection = $this->connect();
        if ($databaseConnection->connect_error) {
            return true;
        }

        $sql = "Select * from user where email = ?";
        $sqlStatement = mysqli_stmt_init($databaseConnection);

        if (!mysqli_stmt_prepare($sqlStatement,$sql))
            {
                return true;
            }
        else
            {
                mysqli_stmt_bind_param($sqlStatement,'s',$email);
                mysqli_stmt_execute($sqlStatement);

                mysqli_stmt_store_result($sqlStatement);

                $resultOfQueryAsRowNumbers = mysqli_stmt_num_rows($sqlStatement);


                if ($this->checkIfUserExists($resultOfQueryAsRowNumbers))
                {
                    $sqlStatement->close();
                    $databaseConnection->close();
                    return true;
                }
                else {
                    $sqlStatement->close();
                    $databaseConnection->close();
                    return false;

            }
        }
    }


    public function registerNewAccount($email,$password,$firstName)
    {
        if($this->checkForExistingEmail($email))
        {
            return true;
        }

        $databaseConnection = $this->connect();

        if ($databaseConnection->connect_error) {
            return true;
        }

        $sql = "Insert into User (FirstName,email,password) VALUES (?,?,?)";
        $sqlStatement = mysqli_stmt_init($databaseConnection);

        if (!mysqli_stmt_prepare($sqlStatement,$sql))
            {
                return true;
            }
        else
            {
                $hashedPassword =password_hash($password,PASSWORD_DEFAULT);

                mysqli_stmt_bind_param($sqlStatement,'sss',$firstName,$email,$hashedPassword);
                mysqli_stmt_execute($sqlStatement);


                $_SESSION['Email'] = $email;
                $_SESSION['FirstName'] = $firstName;
                $_SESSION['LastName'] = "";
                $_SESSION['age'] = 0;

                $sqlStatement->close();
                $databaseConnection->close();

                return false;
            }
        }





    function checkIfUserExists($results)
    {
        return $results>0;
    }

    public function updateAccount($firstName, $lastName, $age, $email)
    {
        $databaseConnection = $this->connect();

        if ($databaseConnection->connect_error) {
            return true;
        }

        $sql = "Update User set FirstName = ?,lastName =?,age =? where email = ?";
        $sqlStatement = mysqli_stmt_init($databaseConnection);

        if (!mysqli_stmt_prepare($sqlStatement,$sql))
        {
            return true;
        }
        else
        {

            mysqli_stmt_bind_param($sqlStatement,'ssis',$firstName,$lastName,$age,$email);
            mysqli_stmt_execute($sqlStatement);


            $_SESSION['Email'] = $email;
            $_SESSION['FirstName'] = $firstName;
            $_SESSION['LastName'] = $lastName;
            $_SESSION['age'] = $age;

            $sqlStatement->close();
            $databaseConnection->close();

            return false;
        }
    }


}