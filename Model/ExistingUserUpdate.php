<?php
require_once "../classes.php";


class ExistingUserUpdate extends User
{
    function __construct($firstName,$lastName,$age)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->age = $age;

        $email  = $_SESSION["Email"];
        parent::__construct($email);
    }

    function update()
    {

        $dbConnections = new DatabaseConn();
        if($dbConnections->updateAccount($this->firstName,$this->lastName,$this->age,$this->email))
        {
            return false;
        }
        else
        {
            return true;
        }

    }

}