<?php

include_once "../classes.php";
class NewUser extends User
{
    function __construct($email,$password,$firstName)
    {
        $this->password = $password;
        $this->firstName = $firstName;
        parent::__construct($email);
    }

    function create()
    {

        $dbConnections = new DatabaseConn();
        if($dbConnections->registerNewAccount($this->email,$this->password,$this->firstName))
        {
            return false;
        }
        else
        {
            return true;
        }

    }
}