<?php

require_once "../classes.php";
class ExistingUser extends User
{
    function __construct($email,$password)
    {
        $this->password = $password;

        parent::__construct($email);
    }

    function authenticate()
    {
        $db = new DatabaseConn();
        if($db->authenticateUserCredentials($this->email,$this->password))
        {
            return false;
        }
        else
        {
            return true;
        }

    }
}